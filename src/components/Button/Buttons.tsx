import { Button } from 'antd'
import React from 'react'

interface ButtonGridProps {
  NameButton: string
}

function ButtonGrid(props: ButtonGridProps) {
  const { NameButton } = props
  const buttonStyle = {
    color: '#404258',  
    borderColor: '#E4E4E8', 
    borderRadius: '20px',
  }

  return (
    <Button style={buttonStyle} className="text text-[16px] rounded-full h-[50px] mb-6">
      {NameButton}
    </Button>
  )
}

export default ButtonGrid

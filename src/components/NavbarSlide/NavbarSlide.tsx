import React, { useState } from 'react'

function NavbarSlide() {
  const options: string[] = ['All Project', 'Solutions', 'Study Case']
  const [activeItem, setActiveItem] = useState<string>(options[0])

  const handleItemClick = (itemName: string) => {
    setActiveItem(itemName)
  }

  return (
    <nav>
      <ul className="navbar-slide text flex gap-6">
        {options.map((option) => (
          <li key={option}>
            <a
              href="#"
              className={`text-[16px] border-b-4 ${
                activeItem === option
                  ? 'text-red-500 font-semibold border-red-500'
                  : 'text-black font-normal border-transparent'
              }`}
              onClick={() => handleItemClick(option)}
            >
              {option}
            </a>
          </li>
        ))}
      </ul>
    </nav>
  )
}

export default NavbarSlide
